# Docker_fastapi_upload_file

### Description
Project for test, script create user and upload file, add file to user.

### Getting started

```git clone https://gitlab.com/me4533521/docker_fastapi_upload_file.git```

```cd docker_fastapi_upload```

build:
```docker-compose build```

start:
```docker-compose start```

### Usage

### POST ```/get_or_create_user/username```

body: username

response: 200
```json
{
    "id": 1,
    "token": "eb4cd87d-cbde-4314-96ad-8253d067d719",
    "name": "username",
    "musics": [
        {
            "id": 1,
            "token": "b7642eb4-6082-4d18-9cf0-d340aed35dd2",
            "name": "sample-15s"
        }
    ]
}
```
description:
Generate user id, token.

### POST ```/music/upload```

body: 
- user_id: 1
- user_token: eb4cd87d-cbde-4314-96ad-8253d067d719

response: 200
```json
{
    "download_link": "http://127.0.0.1:8000/record?music_id=1&user_id=1"
}
```
### GET ```http://127.0.0.1:8000/record?music_id=1&user_id=1```

params:
- music_id: 1
- user_id: 1

response: 200

Download file

### License
For open source projects, say how it is licensed.

### Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
